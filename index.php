<?php

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \Slim\Slim as Slim;
use \coloc\models\Logement;

$db = new DB();

$array = parse_ini_file('src/conf/conf.ini');
$db->addConnection($array);
$db->setAsGlobal();
$db->bootEloquent();

$app = new Slim();

session_start();

$app->get('/', function(){
    $contr = new \coloc\controler\NaviguerControler();
    $contr->afficherHome();
})->name("Home");


$app->get('/afficherUsers',function(){
    $contr = new \coloc\controler\NaviguerControler();
    $contr->listerUtilisateurs();
})->name("Users");

$app->get('/user/:id', function($id){
   $contr = new \coloc\controler\NaviguerControler();
   $contr->detailUser($id);
});

$app->get('/afficherLogements', function(){
   $contr = new \coloc\controler\NaviguerControler();
   $contr->afficherLogements();
})->name("Logements");

$app->get('/logement/:id', function($id){
    $contr = new \coloc\controler\NaviguerControler();
    $contr->detailLogement($id);
});

$app->get('/authentification/', function(){
   $contr = new \coloc\controler\NaviguerControler();
   $contr->auth();
});

$app->get('/deco/', function(){
    $contr = new \coloc\controler\NaviguerControler();
    $contr->deco();
});


$app->get('/authentification/intermediaire/:id', function($id){
   $contr = new \coloc\controler\NaviguerControler();
   $contr->intermed($id);
});

$app->get('/backoffice/', function(){
    $contr = new \coloc\controler\BackofficeControler();
    $contr->homeBackoffice();
});

$app->get('/NewGroupe', function(){
   $contr = new \coloc\controler\GroupeControler();
   $contr->createGroupe();
})->name("NewGroupe");

$app->post('/recuperationDescr/:id', function($id){
    $contr = new \coloc\controler\GroupeControler();
    $contr->recuperation($id);
});










$app->run();