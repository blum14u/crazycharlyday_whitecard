<?php
/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 09/02/2017
 * Time: 17:44
 */

namespace coloc\controler;


use coloc\models\Groupe;
use coloc\view\VueGroupe;

class GroupeControler{

    //fonctionnalité 7
    public function createGroupe(){
        $q = Groupe::all();
        $vue = new VueGroupe($q);
        echo $vue->render(1);
    }

    public function recuperation($id){
        $q = Groupe::where('id','=', $id)->first();
        $vue = new VueGroupe($q);
        echo $vue->render(2);
    }
}