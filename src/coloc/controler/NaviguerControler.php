<?php

namespace coloc\controler;

use coloc\models\Logement;
use coloc\models\User;
use \coloc\view\VueNaviguer as VueNaviguer;

class NaviguerControler{

    //fonctionnalité 1
    public function afficherHome(){
        $vue = new VueNaviguer();
        if(isset($_SESSION['proprio']))
            echo $vue->render(12);
        else
            echo $vue->render(1);
    }

    public function auth(){
        $q = User::all();
        $vue = new VueNaviguer($q);
        echo $vue->render(11);
    }

    public function deco(){
        $vue = new VueNaviguer();
        echo $vue->render(14);
    }

    public function intermed($id){
        $q = User::where('id','=',$id)->first();
        $vue = new VueNaviguer($q);
        echo $vue->render(13);
    }

    //fonctionnalité 3
    public function listerUtilisateurs(){
        $q = User::all();
        $vue = new VueNaviguer($q);
        echo $vue->render(3);
    }

    //fonctionnalité 4
    public function detailUser($id){
        $q = User::where('id','=',$id)->first();
        $vue = new VueNaviguer($q);
        echo $vue->render(4);
    }

    //fonctionnalité 5
    public function afficherLogements(){
        $q = Logement::all();
        $vue = new VueNaviguer($q);
        echo $vue->render(5);
    }

    //fonctionnalité 6
    public function detailLogement($id){
        $q = Logement::where('id','=',$id)->first();
        $vue = new VueNaviguer($q);
        echo $vue->render(6);
    }

}