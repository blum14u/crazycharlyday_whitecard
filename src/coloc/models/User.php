<?php

namespace coloc\models;
use \Illuminate\Database\Eloquent\Model as Model;

class User extends Model{
    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;
}