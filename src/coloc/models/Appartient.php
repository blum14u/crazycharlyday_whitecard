<?php
/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 09/02/2017
 * Time: 17:33
 */

namespace coloc\models;

use \Illuminate\Database\Eloquent\Model as Model;

class Appartient extends Model{

    protected $table = 'appartient';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function membres(){
        return $this->hasMany('coloc\models\User', 'id');
    }
}