<?php


namespace coloc\models;
use \Illuminate\Database\Eloquent\Model as Model;

class Logement extends Model{
    protected $table = 'logement';
    protected $primaryKey = 'id';
    public $timestamps = false;
}