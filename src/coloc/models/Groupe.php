<?php
/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 09/02/2017
 * Time: 17:32
 */

namespace coloc\models;

use \Illuminate\Database\Eloquent\Model as Model;


class Groupe extends Model{

    protected $table = 'groupe';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function appartien(){
        return $this->hasMany('coloc\models\Appartient','id');
    }

}