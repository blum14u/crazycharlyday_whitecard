<?php
/**
 * Created by PhpStorm.
 * User: Hermine
 * Date: 09/02/2017
 * Time: 17:44
 */

namespace coloc\view;


use coloc\models\Groupe;
use Slim\Slim;

class VueGroupe{

    private $array;
    private $app;

    public function __construct($arrayGpe){
        $this->array = $arrayGpe;
        $this->app = Slim::getInstance();
    }

    private function afficherHome(){
        $var = "<div class='container'><div class='explication'><h1>Qu'est ce que c'est ?</h1>";
        $var .= "<p>“La Coloc en Ligne“ est l’application web pour organiser les colocations d’étudiants dans le projet 
                    “Un Toit pour Tous”. La plateforme propose un certain nombre de logements disponibles à la 
                    colocation, à destination d’étudiants, et l’application permet à ses utilisateurs/membres de se 
                    grouper en vue de se proposer comme colocataires d’un logement</p></div>";
        $var.= "<div class='row explication1'>";
        $var .= "<center><ul class='detailUser'>";
        $var .= "<a href='#'><li class='thumbnail'>Se connecter</li></a>";
        $var .= "<a href='authentification/'><li class='thumbnail'>Qui êtes-vous?</li></a>";
        $var .= "</div>";
        $var.= "</ul></center>";
        $var.= "<a href='backoffice/'>Acces Backoffice</a>";

        return $var;
    }

    private function createGroupe(){
        $var = "<div class='page-header'>";
        if(!isset($_SESSION['groupe'])){
            $_SESSION['groupe'] = new Groupe();
            $_SESSION['groupe']->idProprietaire = $_SESSION['proprio'];
            $_SESSION['groupe']->save();
            $var .= "<form method='post' action='recuperationDescr/".$_SESSION['groupe']->id."'>";
            $var .= "description du groupe";
            $var .= "<textarea name='description' rows='4' cols='30' required></textarea>";
            $var .= "<input type='submit' value='valider'/>";
            $var .= "</form>";
        }
        $var .= "</div>";
        return $var;
    }

    private function recuperation(){
        $postDescr = $this->app->request->post('description');

        $groupe = $this->array;
        $groupe->descr = $postDescr;
        $groupe->save();

        $var = "<h1>Votre groupe a bien été créé !</h1>";
        $var .= "<center><ul class='detailUser'>";
        $var.= "<a href='../'><li class='thumbnail'>Retour à la page d'accueil</li></a>";
        $var.= "</ul></center>";
        return $var;
    }

    public function render($id){
        switch($id){
            case 1 :
                $content = $this->createGroupe();
                break;
            case 2 :
                $content = $this->recuperation();
                break;
            default:
                $content = $this->afficherHome();
                break;
        }
        $app = Slim::getInstance();
        $urlHome = $app->urlFor("Home");
        $urlListeUsers = $app->urlFor("Users");
        $urlListeLogements = $app->urlFor("Logements");
        $urlNewGroupe = $app->urlFor("NewGroupe");
        $html = <<<END
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../web/css/style.css">
    <title>Coloc en ligne</title>
</head>
            <body>
<div class="page-header">
                <div style='background-color: blue; width:100%; height: 10px'></div>
                     <h1 class="text-center" style="padding-top:1%">Coloc en ligne</h1>
                </div>
                <div style='background-color: blue; width:100%; height: 10px'></div>
                    <ul style='background-color: #28a4c9' class="nav nav-pills">
                         <li role="presentation" class="active"><a href="$urlHome">Home</a></li>
                         <li role="presentation"><a href="$urlListeUsers">Utilisateurs</a></li>
                         <li role="presentation"><a href="$urlListeLogements">Logements</a></li>
                         <li role="presentation"><a href="$urlNewGroupe">Créer un groupe</a></li>                        
                        </ul>
                <div style='background-color: blue; width:100%; height: 10px'></div>
                <div id='containerListeP' class="container">
                    $content
                 </div>
                 <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="crossorigin="anonymous"></script>
                 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
            </body>
<html>
END;
        return $html;
    }

}