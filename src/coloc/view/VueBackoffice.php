<?php

namespace coloc\view;

use Slim\Slim;

class VueBackoffice{

    private $array;

    public function __construct($arrayNav = null){
        $this->array = $arrayNav;
    }

    private function afficherAcceuil(){
        $var = "<div class='container'><div class='explication'><h1>VOUS ETES SUR LA PAGE DE GESTION DU SITE</h1>";
        $var .= "<p>Bienvenue sur la page qu'on a pas eu le temps de faire, mais qu'on avait vraiment envie de faire.</p></div>";
        $var.= "<div class='row explication1'>";
        $var .= "<center><ul class='detailUser'>";
        $var .= "<a href='#'><li class='thumbnail'>Se connecter</li></a>";
        $var .= "</div>";
        $var.= "</ul></center>";

        return $var;
    }



    public function render($id){
        switch($id){
            case 1 :
                $content = $this->afficherAcceuil();
                break;
        }

        $app = Slim::getInstance();
        $urlHome = $app->urlFor("Home");
        $urlListeUsers = $app->urlFor("Users");
        $urlListeLogements = $app->urlFor("Logements");
        $html = <<<END
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../../web/css/style.css">
    <title>Backoffice</title>
</head>
            <body>
<div class="page-header">
                <div style='background-color: purple; width:100%; height: 10px'></div>
                     <h1 class="text-center" style="padding-top:1%">Backoffice</h1>
                </div>
                <div style='background-color: purple; width:100%; height: 10px'></div>
                    <ul style='background-color: #28a4c9' class="nav nav-pills">
                         <li role="presentation" class="active"><a href="$urlHome">Home</a></li>
                        </ul>
                <div style='background-color: purple; width:100%; height: 10px'></div>
                <div id='containerListeP' class="container">
                    $content
                 </div>
                 <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="crossorigin="anonymous"></script>
                 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
            </body>
<html>
END;
        return $html;
    }



}