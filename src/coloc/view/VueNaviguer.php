<?php

namespace coloc\view;

use coloc\models\User;
use Slim\Slim;

class VueNaviguer{

    private $array;

    public function __construct($arrayNav = null){
        $this->array = $arrayNav;
    }

    private function afficherHome(){
        $var = "<div class='container'><div class='explication'><h1>Qu'est ce que c'est ?</h1>";
        $var .= "<p>“La Coloc en Ligne“ est l’application web pour organiser les colocations d’étudiants dans le projet 
                    “Un Toit pour Tous”. La plateforme propose un certain nombre de logements disponibles à la 
                    colocation, à destination d’étudiants, et l’application permet à ses utilisateurs/membres de se 
                    grouper en vue de se proposer comme colocataires d’un logement</p></div>";
        $var.= "<div class='row explication1'>";
        $var .= "<center><ul class='detailUser'>";
        $var .= "<a href='#'><li class='thumbnail'>Se connecter</li></a>";
        $var .= "<a href='authentification/'><li class='thumbnail'>Qui êtes-vous?</li></a>";
        $var .= "</div>";
        $var.= "</ul></center>";
        $var.= "<a href='backoffice/'>Acces Backoffice</a>";

        return $var;
    }

    private function afficherHomeAuthentifie(){
        $user = User::where('id','=',$_SESSION['proprio'])->first();
        $var = "<div class='container'><div class='explication'><h1>Qu'est ce que c'est ?</h1>";
        $var .= "<p>La “Coloc en Ligne“ est l’application web pour organiser les colocations d’étudiants dans le projet 
                    “Un Toit pour Tous”. La plateforme propose un certain nombre de logements disponibles à la 
                    colocation, à destination d’étudiants, et l’application permet à ses utilisateurs/membres de se 
                    grouper en vue de se proposer comme colocataires d’un logement</p></div>";
        $var.= "<div class='row explication1'>";
        $var .= "<center><ul class='detailUser'>";
        $var .= "<li class='thumbnail'>Bonjour $user->nom !</li>";
        $var .= "<a href='deco/'><li style='background-color: coral' class='thumbnail'>Se Déconnecter</li></a>";
        $var .= "</div>";
        $var.= "</ul></center>";
        $var.= "<a href='backoffice/'>Acces Backoffice</a>";
        return $var;
    }

    private function auth(){
        $var = " <ul style='background-color: #337ab7; width: 100%' class='nav navbar-nav navbar-left'>";
        $var .= "<center><h1>Qui êtes-vous?</h1></center>";
        $var .= "<center><ul class='detailUser'>";
        foreach ($this->array as $Users) {
            $var.= "<div class='col-lg-3 col-md-4 col-xs-6 thumb tailleThumb'>";
            $var.= "<a href='intermediaire/$Users->id'><img class='tailleImage' class='vignette' src='../../web/img/user/$Users->id.jpg' /></a>";
            $var.= "<li class='thumbnail'>$Users->nom</li>";
            $var.= "</div>";
        }
        $var.= "</ul></center>";

        return $var;
    }

    private function intermed(){
        $user = $this->array;
        if(!isset($_SESSION['proprio']))
            $_SESSION['proprio'] = $user->id;
        $var = "<h1> Vous êtes bien connecté(e) en tant que $user->nom</h1>";
        $var .= "<center><ul class='detailUser'>";
        $var.= "<a href='../../'><li class='thumbnail'>Retour à la page d'accueil</li></a>";
        $var.= "</ul></center>";
        return $var;
    }

    private function deco(){
        $user = $this->array;
        if(isset($_SESSION['proprio']))
            unset($_SESSION['proprio']);
        $var = "<h1> Vous êtes maintenant déconnecté</h1>";
        $var .= "<center><ul class='detailUser'>";
        $var.= "<a href='../'><li class='thumbnail'>Retour à la page d'accueil</li></a>";
        $var.= "</ul></center>";
        return $var;
    }

    private function afficherUsers(){
        $var = " <ul style='background-color: #978f98; width: 100%' class='nav navbar-nav navbar-left'>";
        $var .= "<center><ul class='detailUser'>";
        foreach ($this->array as $Users) {
            $var.= "<div class='col-lg-3 col-md-4 col-xs-6 thumb tailleThumb'>";
            $var.= "<a href='user/".$Users->id."'><img class='tailleImage' class='vignette' src='../web/img/user/$Users->id.jpg' /></a>";
            $var.= "<li class='thumbnail'>$Users->nom</li>";
            $var.= "</div>";
        }
        $var.= "</ul></center>";
        return $var;
    }

    private function detailUser(){
        $a =$this->array;
        $var ="<div class='container afficherDetailsPrestation'>";
        $var .= "<h1 style='text-decoration: underline'> Voici la description de $a->nom : </h1>";
        $var .= $a->nom.'  '.$a->message;
        $var  .='<center><span>';
        $var .="<img class='tailleImage imagePresta' src='../../web/img/user/$a->id.jpg' />";
        $var .='</span></center>';
        $var .= "</div>";
        return $var;
    }

    private function afficherLogements(){
        $var = " <ul style='background-color: #337ab7; width: 100%' class='nav navbar-nav navbar-left'>";
        $var .= "<center><ul class='detailUser'>";
        foreach($this->array as $Logements){
            if($Logements->places>0){
                $var.= "<div class='col-lg-3 col-md-4 col-xs-6 thumb tailleThumb'>";
                $var.= "<a href='logement/".$Logements->id."'><img class='tailleImage' class='vignette' src='../web/img/apart/$Logements->id.jpg' /></a>";
                if($Logements->places == 1)
                    $var.= "<li class='thumbnail'>Il y a 1 place dans cet appart</li>";
                else
                    $var.= "<li class='thumbnail'>Il y a $Logements->places places dans cet appart</li></a>";
                $var.= "</div>";
            }
        }
        $var.= "</ul></center>";
        return $var;
    }

    private function detailLogement(){
        $a =$this->array;
        $var ="<div class='container afficherDetailsPrestation'>";
        $var .= "<h1 style='text-decoration: underline'> Voici la description du logement numéro $a->id : </h1>";
        $var .= "Ce logement possède ".$a->places." places"; // MODIFIER AVEC LES FUTURES DESCRIPTIONS
        $var  .='<center><span>';
        $var .="<img class='tailleImage imagePresta' src='../../web/img/apart/$a->id.jpg' />";
        $var .='</span></center>';
        $var .= "</div>";
        return $var;
    }


    public function render($id){
        switch($id){
            case 1 :
                $content = $this->afficherHome();
                break;
            case 11 :
                $content = $this->auth();
                break;
            case 12 :
                $content = $this->afficherHomeAuthentifie();
                break;
            case 13 :
                $content = $this->intermed();
                break;
            case 14 :
                $content = $this->deco();
                break;
            case 3 :
                $content = $this->afficherUsers();
                break;
            case 4:
                $content = $this->detailUser();
                break;
            case 5:
                $content = $this->afficherLogements();
                break;
            case 6:
                $content = $this->detailLogement();
                break;
            default :
                $content = $this->afficherHome();
                break;
        }

        $app = Slim::getInstance();
        $urlHome = $app->urlFor("Home");
        $urlListeUsers = $app->urlFor("Users");
        $urlListeLogements = $app->urlFor("Logements");
        $urlNewGroupe = $app->urlFor("NewGroupe");
        $html = <<<END
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../web/css/style.css">
    <title>Coloc en ligne</title>
</head>
            <body>
<div class="page-header">
                <div style='background-color: blue; width:100%; height: 10px'></div>
                     <h1 class="text-center" style="padding-top:1%">Coloc en ligne</h1>
                </div>
                <div style='background-color: blue; width:100%; height: 10px'></div>
                    <ul style='background-color: #28a4c9' class="nav nav-pills">
                         <li role="presentation" class="active"><a href="$urlHome">Home</a></li>
                         <li role="presentation"><a href="$urlListeUsers">Utilisateurs</a></li>
                         <li role="presentation"><a href="$urlListeLogements">Logements</a></li>
                         <li role="presentation"><a href="$urlNewGroupe">Créer un groupe</a></li>                                           
                        </ul>
                <div style='background-color: blue; width:100%; height: 10px'></div>
                <div id='containerListeP' class="container">
                    $content
                 </div>
                 <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="crossorigin="anonymous"></script>
                 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
            </body>
<html>
END;
    return $html;
    }

}